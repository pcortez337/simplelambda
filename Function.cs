using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

using Amazon.Lambda.Core;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace SimpleLambda
{
    public class Function
    {
        
        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool FunctionHandler(ILambdaContext context)
        {
            //return input?.ToUpper();

            var isSuccess = false;
            context.Logger.Log("Begin function");

            var invoiceIdList = new List<string>();

            try
            {

                using (var conn = new SqlConnection(System.Environment.GetEnvironmentVariable("championProductsDBConnectionString")))
                {
                    using (var cmd = new SqlCommand($"SELECT invoiceId from invoice", conn))
                    {
                        conn.Open();

                        SqlDataReader rdr = cmd.ExecuteReader();

                        //loop through the results
                        while (rdr.Read())
                        {
                            invoiceIdList.Add(rdr[0].ToString());
                        }
                        rdr.Close();
                        
                    }
                    ProcessInvoiceItems(invoiceIdList, conn);
                }
                
            }
            catch (Exception ex)
            {
                isSuccess = false;
            }
            return isSuccess;
        }

        private void ProcessInvoiceItems(List<string> invoiceIdList, SqlConnection conn)
        {
            foreach (var item in invoiceIdList)
            {
                using (var updateCmd = new SqlCommand($"UPDATE dbo.Invoice SET PaidDate = GETDATE() WHERE invoiceId = " + item, conn))
                {
                    updateCmd.ExecuteNonQuery();
                }
            }

        }
    }
}
